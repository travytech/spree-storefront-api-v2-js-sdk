import { POST } from '../constants'
import { authParams, refreshParams } from '../helpers/auth'
import Http from '../Http'
import { AuthTokenAttr, RefreshTokenAttr, EncryptedData, VerifyOtpAttr } from '../interfaces/Authentication'
import { IOtpResult, IToken, ITokenResult } from '../interfaces/Token'
import { Routes } from '../routes'

export default class Authentication extends Http {
  public async getToken(params: AuthTokenAttr): Promise<ITokenResult> {
    return await this.spreeResponse(POST, Routes.oauthTokenPath(), {}, authParams(params)) as ITokenResult
  }
  public async refreshToken(params: RefreshTokenAttr): Promise<ITokenResult> {
    return await this.spreeResponse(POST, Routes.oauthTokenPath(), {}, refreshParams(params)) as ITokenResult
  }
  public async requestOtp(token: IToken, params: EncryptedData): Promise<IOtpResult> {
    return await this.spreeResponse(POST, Routes.userRequestOtp(), token, params) as IOtpResult
  }
  public async verifyOtp(token: IToken, params: VerifyOtpAttr): Promise<IOtpResult> {
    return await this.spreeResponse(POST, Routes.userVerifyOtp(), token, params) as IOtpResult
  }
}
