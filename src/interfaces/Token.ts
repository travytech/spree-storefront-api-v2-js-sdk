import { JsonApiDocument, JsonApiSingleResponse } from './JsonApi'
import { IRelationships } from './Relationships'
import { ResultResponse } from './ResultResponse'

export interface IToken {
  orderToken?: string
  bearerToken?: string
  access_token?: string
  token_type?: string
  expires_in?: number
  refresh_token?: string
  created_at?: Date
}

export interface ITokenResult extends ResultResponse<IToken> {}

export interface IOtpAttr extends JsonApiDocument {
  data: {
    id: string
    type: string
    attributes: {
      verification_code: string
      verified_at?: Date
      is_verified: boolean
    }
    relationships: IRelationships
  }
}

export interface IOtp extends JsonApiSingleResponse {
  data: IOtpAttr
}

export interface IOtpResult extends ResultResponse<IOtp> {}
