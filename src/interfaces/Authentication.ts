export interface AuthTokenAttr {
  username: string
  password: string
}

export interface RefreshTokenAttr {
  refresh_token: string
}

export interface VerifyOtpAttr {
  otp: string
}

export interface EncryptedData {
  q: string
}