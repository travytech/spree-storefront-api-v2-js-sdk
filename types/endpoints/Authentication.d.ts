import Http from '../Http';
import { AuthTokenAttr, RefreshTokenAttr, EncryptedData, VerifyOtpAttr } from '../interfaces/Authentication';
import { IOtpResult, IToken, ITokenResult } from '../interfaces/Token';
export default class Authentication extends Http {
    getToken(params: AuthTokenAttr): Promise<ITokenResult>;
    refreshToken(params: RefreshTokenAttr): Promise<ITokenResult>;
    requestOtp(token: IToken, params: EncryptedData): Promise<IOtpResult>;
    verifyOtp(token: IToken, params: VerifyOtpAttr): Promise<IOtpResult>;
}
